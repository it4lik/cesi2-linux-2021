# Partie 3 : Maintien en condition opérationnelle

Ici aussi, on aurait pu aborder plein de choses, on va voir ici deux aspects :

- monitoring
  - on va utiliser [Netdata](https://www.netdata.cloud/)
- backup
  - un script maison qui fait appel à [Borg](https://borgbackup.readthedocs.io/en/stable/)

## Sommaire

- [Partie 3 : Maintien en condition opérationnelle](#partie-3--maintien-en-condition-opérationnelle)
  - [Sommaire](#sommaire)
- [I. Monitoring](#i-monitoring)
  - [1. Intro](#1-intro)
  - [2. Setup Netdata](#2-setup-netdata)
  - [3. Bonus : alerting](#3-bonus--alerting)
  - [4. Bonus : proxying](#4-bonus--proxying)
- [II. Backup](#ii-backup)

# I. Monitoring

## 1. Intro

[Netdata](https://www.netdata.cloud/) a plusieurs fonctions :

- **récolteur de données**
  - il récolte en temps réel des données sur le systèmes (utilisation CPU, RAM, fichiers de logs, etc)
- **interface web**
  - visualisation sexy en temps réel des données
- **alerting**
  - envoie des alertes, plein de choses compatibles comme Discord

> Discord ou autre chose, les systèmes de chat sont particulièrement adaptés à l'alerting pour avoir quelque chose de structuré, plutôt que le typique alerting par mail

**Netdata est simple de setup, très performant, et modulaire :**

- en effet, on va s'en servir pour tout faire, mais il peut s'inscrire comme une brique dans des infras plus grandes
- il est réputé pour être un récolteur de données très performant : beaucoups de métriques récoltées, en peu de temps, pour un faible coût en perfs (on parle de "faible overhead")
- ces métriques peuvent éventuellement être exportées vers un outil qui les centralisent

## 2. Setup Netdata

> Sur quelle machine ? A terme, toutes dans l'idéale, je ne vous l'imposerai pas ici, libre à vous :) Commencez au moins par une, pour prendre en main.

🌞 **Installez Netdata** en exécutant la commande suivante :

```bash
$ bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)
```

> La méthode d'installation est peu orthodoxe, n'hésitez pas à me demander si vous voulez en savoir plus.

🌞 **Démarrez Netdata**

- vous avez un service `netdata`
- ouvrez le port dans le firewall 19999/tcp : c'est l'interface Web de Netdata (vous pouvez le vérifier avec une commande `ss` comme toujours !)

Bah voilà hein ! Ouvrez votre navigateur, go `http://<IP_VM>:19999` and enjoy.  
Prenez le temps de regarder un peu les métriques remontées. Certaines choses seront différentes suivant la machine où vous l'installez !

## 3. Bonus : alerting

🌞 **Mettez en place un alerting Discord**

- [lien de la doc](https://learn.netdata.cloud/docs/agent/health/notifications/discord) pour l'alerting Discord
- le principe est de recevoir les alertes dans un salon texte dédié sur un serveur Discord

## 4. Bonus : proxying

Euh bah là on a Apache protégé par NGINX, mais le serveur Web à moitié naze que Netdata utilise, pas de soucis ? BAH SI c'est un soucis :'(

🌞 **Ajustez la conf**

- l'interface web de Netdata doit être joignable par le reverse proxy

# II. Backup

[Borg](https://borgbackup.readthedocs.io/en/stable/) est un outil qui permet de réaliser des sauvegardes.

Il est performant et sécurisé :

- performant à l'utilisation
  - création de backup rapide, idem pour restauration
- les données sauvegardées sont chiffrées
- les données sauvegardées sont dédupliquées

Il repose sur un concept simple :

- on créer un "dépôt Borg" sur la machine avec une commande `borg init`
- c'est dans ce dépôt que pourront être stockées des sauvegardes
- on peut ensuite déclencher une sauvegarde d'un dossier donné vers le dépôt avec une commande `borg create`

Nous, on va écrire un script :

- il créer une nouvelle sauvegarde borg (avec `borg create` donc) d'un dossier précis, avec un nommage précis, dans un dépôt précis
- on en fera ensuite un service : un p'tit `backup.service`
- puis on fera en sorte que ce service se lance à intervalles réguliers

🌞 **Téléchargez Borg** sur la machine `web.tp2.cesi`

- téléchargez avec les commandes suivantes :

```bash
$ cd
$ curl -SLO https://github.com/borgbackup/borg/releases/download/1.1.17/borg-linux64
$ sudo cp borg-linux64 /usr/local/bin/borg
$ sudo chown root:root /usr/local/bin/borg
$ sudo chmod 755 /usr/local/bin/borg
```

🌞 **Jouer avec Borg**

- créez un dépôt, faites des sauvegardes, restaures des sauvegardes, testez l'outil un peu

🌞 **Ecrire un script**

- il doit sauvegarder le dossier où se trouve NextCloud
- le dépôt borg doit être dans le dossier `/srv/backup/`
- le nom de la sauvegarde doit être `nextcloud_YYMMDD_HHMMSS`
- testez le à la main, avant de continuer

🌞 **Créer un service**

- créer un service `backup_db.service` qui exécute votre script
- ainsi, quand on lance le service, une backup de la base de données est déclenchée

La syntaxe, toujours la même :

```bash
[Unit]
Description=<DESCRIPTION>

[Service]
ExecStart=<COMMAND>
Type=oneshot

[Install]
WantedBy=multi-user.target
```

**NB : vous DEVEZ ajoutez la ligne `Type=oneshot` en dessous de la ligne `ExecStart=` dans votre service**. Cela indique au système que ce service ne sera pas un démon qui s'exécute en permanence, mais un script s'eécutera, puis qui aura une fin d'exécution (sinon le système peut par exemple essayer de relancer automatiquement un service qui s'arrête).

🌞 **Créer un timer**

- un *timer* c'est un fichier qui permet d'exécuter un service à intervalles réguliers
- créez un *timer* qui exécute le service `backup` toutes les heures

Pour cela, créer le fichier `/etc/systemd/system/backup.timer`.

> Notez qu'il est dans le même dossier que le service, et qu'il porte le même nom, mais pas la même extension.

Contenu du fichier `/etc/systemd/system/backup.timer` :

```bash
[Unit]
Description=Lance backup.service à intervalles réguliers
Requires=backup.service

[Timer]
Unit=backup.service
OnCalendar=hourly

[Install]
WantedBy=timers.target
```

> Au niveau du `OnCalendar`, on précise tout un tas de trucs : une heure précise une fois par semaine, toutes les trois heures les jours pairs (ui c'est improbable, mais on peut !), etc.

Activez maintenant le *timer* avec :

```bash
# on indique qu'on a modifié la conf du système
$ sudo systemctl daemon-reload

# démarrage immédiat du timer
$ sudo systemctl start backup.timer

# activation automatique du timer au boot de la machine
$ sudo systemctl enable backup.timer
```

🌞 **Vérifier que le *timer* a été pris en compte**, en affichant l'heure de sa prochaine exécution :

```bash
$ sudo systemctl list-timers
```
