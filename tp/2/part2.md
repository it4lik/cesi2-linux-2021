# Partie 2 : Sécurisation

On pourrait aborder beaucoup, beaucoup, beaucouuuuup de choses à ce sujet. De notre côté on va aller sur les aspects élémentaires.

> **QUEWA ?!** Vous voulez un truc complet sur comment sécuriser notre machin ? OK :D Le security guide de la doc officielle RedHat [**ICI**](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/security_guide/index) et le TRES bon "benchmark" de CIS Security [**ICI**](https://www.cisecurity.org/benchmark/centos_linux/). Gardez ça de côté pour quand vous aurez du temps pour lire ; en plus d'être des guides de sécurité, ce sont d'incroyables sources de connaissances.

## Sommaire

- [Partie 2 : Sécurisation](#partie-2--sécurisation)
  - [Sommaire](#sommaire)
- [I. Serveur SSH](#i-serveur-ssh)
  - [1. Conf SSH](#1-conf-ssh)
  - [2. Bonus : Fail2Ban](#2-bonus--fail2ban)
- [II. Serveur Web](#ii-serveur-web)
  - [1. Reverse Proxy](#1-reverse-proxy)
  - [2. HTTPS](#2-https)

# I. Serveur SSH

Dans cette partie on va renforcer un peu la sécurité autour du serveur SSH. Cela va se faire essentiellement au travers du fichier de conf `/etc/ssh/sshd_config`.

> L'ANSSI avait édité un guide de sécurité vraiment pas mal pour les serveurs SSH. Je parle au passé parce qu'il est un peu outdated (2015), [je vous le link quand même](https://www.ssi.gouv.fr/uploads/2014/01/NT_OpenSSH.pdf), en vrai la plupart des points restent d'actualité. Simplement, pour ce qui est des algos de chiffrement utilisés, ou la longueurs des clés conseillées, c'est plus vraiment à jour :(

## 1. Conf SSH

Si c'est pas fait vous **devez maintenant** configurer un accès par clés pour SSH.

🌞 **Modifier la conf du serveur SSH**

- désactiver la connexion de root
- désactiver la connexion par mot de passe et forcer la connexion par clés
- forcer l'utilisation d'algorithmes de chiffrement forts
  - [hop un petit article qui parle de ça](https://farrokhi.net/posts/2020/05/securing-sshd-configuration/)

> Il est souvent recommandé de changer le port par défaut sur lequel écoute SSH. Ce n'est utile quasiment que si vous exposez votre serveur SSH sur une IP publique. En effet, changer le port vous protégera des attaques de masse (botnets), mais pas d'attaques ciblées (suffit de faire un scan de ports pour trouver le serveur SSH).

## 2. Bonus : Fail2Ban

Fail2Ban est un outil très simple :

- il lit les fichiers de logs en temps réel
- il repère des lignes en particulier, en fonction de patterns qu'on a défini
- si des lignes correspondant au même pattern sont répétées trop de fois, il effectue une action

Le cas typique :

- serveur SSH
- get attacked :(
- fail2ban détecte l'attaque rapidement, et ban l'IP source

Ca permet de se prémunir là encore d'attaques de masse : il faut que ça spam pour que fail2ban agisse. Des attaques qui essaient de bruteforce l'accès typiquement.

> C'est pas du tout que dans les séries Netflix hein. Vous mettez une machine avec une IP publique sur internet et c'est très rapide, vous vous faites attaquer toute la journée, tout le temps, plusieurs fois par minutes c'est pas choquant. Plus votre IP reste longtemps avec tel ou tel port ouvert, plus elle est "connue", et les attaques n'iront pas en diminuant.

🌞 **Installez et configurez fail2ban**

- je vous laisse chercher (en anglais sivoplé :( ) par vous-mêmes sur internet
  - [je vous link un random article plutôt ok qui explique le minimum pour SSH](https://www.techrepublic.com/article/how-to-install-fail2ban-on-rocky-linux-and-almalinux/)
- je vous impose pas une conf en particulier, le but c'est de savoir l'utiliser, au moins pour bloquer le spam sur votre serveur SSH
- **testez que ça fonctionne** : spammez un peu les connexions SSH (il faut que la connexion échoue : mauvais mot de passe par exemple), et faites-vous ban :)

> Le lien que j'ai envoyé ne parle pas en détail de comment ça marche. Simplement parce que SSH est très utilisé, et que fail2ban a déjà une pré-configuration pour fonctionner pour ce genre de services : il sait quelle ligne chercher et dans quel fichier. La doc de l'outil et/ou les fichiers de conf par défaut pourront vous renseigner +.

# II. Serveur Web

Bah ui certains l'ont fait en bonus hier, mais comme ça, on met tout le monde à niveau. Libre à vous de sauter ça ou de le refaire (un peu plus en conscience avec le cours sur TLS ?) ou simplement de le faire si c'était pas fait avant !

## 1. Reverse Proxy

**Un reverse proxy c'est un serveur qui sert d'intermédiaire entre un client et un serveur, il est mis en place par l'administrateur du serveur.**

**Il a pour simple rôle de réceptionner les requêtes du client, et de les faire passer au serveur.** Nice, easy, and simple.

Maiiiis il peut évidemment permettre + de choses que ça :

- chiffrement (sécu, perfs)
- caching (perfs)
- répartition de charge (sécu)

**De façon générale, c'est lui qui est le "front" des applications Web**, faut que ce soit une machine robuste et secure. Il permet de centraliser les accès des clients aux applications Web.  
En effet, il n'est pas rare dans l'industrie de voir un nombre restreint de reverse proxies (mais bien bien solides) qui permettent d'accéder à une multitudes d'applications Web.

**N'hésitez pas, comme d'hab, à m'appeler si vous voulez une explication claire à l'oral de tout ça.**

---

🖥️ **Créez une nouvelle machine : `proxy.tp2.cesi`.** 🖥️

- vous déroulerez [la checklist de la section prérequis du TP](./README.md#prérequis) sur cette nouvelle machine

🌞 **Installer NGINX**

- le paquet et le service s'appellent `nginx`

**Quelques infos :**

- les fichiers de log sont dans le dossier `/var/log/nginx/`
- la conf principale est dans `/etc/nginx/nginx.conf`
  - une des dernières lignes inclut des fichiers contenus dans `/etc/nginx/conf.d/`
  - n'hésitez pas à le lire le fichier, et à l'épurer des commentaires inutiles pour y voir plus clair
- n'oubliez pas la commande `sudo journalctl -xe -u <SERVICE>` pour obtenir les logs d'un service donné

🌞 **Configurer NGINX comme reverse proxy**

- **créez un nouveau fichier de conf dans `/etc/nginx/conf.d/` pour mettre votre conf de reverse proxy**
- je vous laisse chercher un peu par vous-mêmes pour ça
- je vous lâche quand même quelques liens :
  - [un article sympa](https://linuxize.com/post/nginx-reverse-proxy/#common-nginx-reverse-proxy-options) qui explique de façon claire la conf mise en place (particulièrement la section sur [les options récurrentes "common options"](https://linuxize.com/post/nginx-reverse-proxy/#common-nginx-reverse-proxy-options))
  - [ce très bon générateur de conf NGINX](https://www.digitalocean.com/community/tools/nginx#?)
    - c'est bien de l'avoir sous le coude, mais faut comprendre ce que ça fait, c'est mieux :)
  - [et pour le plaisir, j'ai pas de mots](https://docs.microsoft.com/en-us/troubleshoot/aspnetcore/2-2-install-nginx-configure-it-reverse-proxy) 
    - quand je l'ai vu, je suis vraiment parti d'une bonne intention, état d'esprit partial, mais non. Les screens dans une doc pour du terminal c'est nan
    - merci Microsoft, toujours le mot pour rire :') (le contenu est à peu près ok, dommage)

🌞 **Une fois en place, text !**

- il faudra de nouveau modifier le fichier hosts de votre PC : bah oui, le nom `web.tp2.cesi` doit pointer vers l'IP du reverse proxy maintenant !
  - mais mais mais, `web.tp2.cesi` qui pointe vers l'IP d'une autre machine, le reverse proxy, c'est pas chelou ?
  - bah non :D

## 2. HTTPS

🌞 **Générer une clé et un certificat avec la commande suivante :**

```bash
$ openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 -keyout server.key -out server.crt

$ ls
server.crt  server.key
```

**Par convention dans les systèmes RedHat :**

- on met les clés privées dans `/etc/pki/tls/private/`
  - permissions très restrictives
- les certificats (clés publiques) dans `/etc/pki/tls/certs/`
  - permissions moins restrictives : généralement y'a le droit de lecture pour tout le monde

Aussi, on nomme souvent le certificat et la clé en fonction du nom de domaine pour lesquels ils sont valides, en l'occurence `web.tp2.cesi.key` pour la clé par exemple.

🌞 **Allez, faut ranger la chambre**

- déplacez au bon endroit et renommez la clé et le certificat

🌞 **Affiner la conf de NGINX**

- direction le fichier de conf de votre reverse proxy
- il faudra ajouter les lignes suivantes

```nginx
# cette ligne 'listen', vous l'avez déjà. Remplacez-la.
listen                  443 ssl http2;

# nouvelles lignes
# remplacez les chemins par la clé et le cert que vous venez de générer
ssl_certificate         /path/to/cert;
ssl_certificate_key     /path/to/key;
```

Eeet bah c'est tout.

🌞 **Test !**

- vérifiez avec votre navigateur que vous accédez en HTTPS à l'application désormais
  - alerte de sécurité : normal, le certificat est auto-signé
- `curl -k` : l'option `-k` vous permettra d'accepter les connexions TLS "non-sécurisée" : le certificat est auto-signé

🌞 **Bonus**

- **ajouter le certificat au magasin de certificat de votre navigateur** pour avoir un pitit cadenas vert
  - très utilisé en entreprise, avec des méthodes automatisées bien sûr : déployer un cert interne sur les postes client pour trust les applications internes
- **ajouter une redirection HTTP -> HTTPS**
  - c'est très courant aussi : on fait en sorte que si un client arrive en HTTP, il soit redirigé vers le site en HTTPS
  - ça permet aux clients qui arrivent en HTTP de pas manger un gros "CONNECTION REFUSED" dans leur navigateur :)
    - [hop un artiiiicle qui parle de ça](https://linuxize.com/post/redirect-http-to-https-in-nginx/) (n'hésitez pas, comme toujours, à chercher par vous-mêmes)
- **renforcer l'échange TLS en sélectionnant les algos de chiffrement à utiliser**
  - cherchez "nginx strong ciphers" :)
- **répartition de charge**
  - clonez la VM Apache + NextCloud
  - mettez en place de la répartition de charge entre les deux machines renommées `web1.tp2.cesi` et `web2 .tp2.cesi`
