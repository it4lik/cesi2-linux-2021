# Partie 1 : Mise en place de la solution

![There is no cloud](./pics/there_is_no_cloud.jpg)

[NextCloud](https://github.com/nextcloud/server) est une application web développée en PHP : on va avoir besoin d'un serveur web qui fait tourner du PHP. On utilisera cette fois le serveur Apache pour ça.

> Plus précisément, il est recommandé de faire tourner NextCloud avec PHP7.4 pour notre installation.

NextCloud a aussi besoin d'une base de données SQL pour fonctionner. Nous utiliserons MariaDB dans le cadre du TP, qui est nativement présent dans les dépôts de Rocky.

Le but de cette partie :

- setup le serveur Web Apache
- setup la base de données Apache
- télécharger NextCloud
  - c'est juste un ensemble de fichiers PHP, JS, CSS, HTML, etc.
  - il faudra déplacer tous ces fichiers dans un dossier spécifique : **la racine web** (ou *webroot*) de notre site
    - c'est pendant la configuration d'Apache que l'on définira à quel chemin se trouve la racine web pour NextCloud
    - tout ça, c'est juste beaucoup de termes pour dire qu'on met NextCloud dans un dossier spécifique. Et on indique à Apache que c'est dans ce dossier que se trouve NextCloud. **easy.**

**Z'est bardi z'est bardiii.**

## Sommaire

- [Partie 1 : Mise en place de la solution](#partie-1--mise-en-place-de-la-solution)
  - [Sommaire](#sommaire)
- [I. Setup base de données](#i-setup-base-de-données)
  - [1. Install MariaDB](#1-install-mariadb)
  - [2. Conf MariaDB](#2-conf-mariadb)
  - [3. Test](#3-test)
- [II. Setup Apache](#ii-setup-apache)
  - [1. Install Apache](#1-install-apache)
    - [A. Apache](#a-apache)
    - [B. PHP](#b-php)
  - [2. Conf Apache](#2-conf-apache)
- [III. NextCloud](#iii-nextcloud)
  - [4. Test](#4-test)

# I. Setup base de données

## 1. Install MariaDB

🌞 **Installer MariaDB sur la machine `db.tp2.cesi`**

- le paquet s'appelle `mariadb-server`

🌞 **Le service MariaDB**

- le service s'appelle `mariadb`
- lancez-le avec une commande `systemctl`
- exécutez la commande `sudo systemctl enable mariadb` pour faire en sorte que MariaDB se lance au démarrage de la machine
- vérifiez qu'il est bien actif avec une commande `systemctl`
- déterminez sur quel port la base de données écoute avec une commande `ss`
- déterminez le(s) processus lié(s) au service MariaDB (commande `ps`)
  - déterminez sous quel utilisateur est lancé le(s) process MariaDB

🌞 **Firewall**

- pour autoriser les connexions qui viendront de la machine `web.tp2.cesi`, il faut conf le firewall
  - ouvrez le port utilisé par MySQL à l'aide d'une commande `firewall-cmd`
  - ce port doit être ouvert sur la machine `db.tp2.cesi` : cela permettra à `web.tp2.cesi` de s'y connecter

> Il y a [le mémo Réseau Rocky](../../cours/memo/rocky_network.md) pour le détail des commandes liées au firewall.

## 2. Conf MariaDB

Première étape : le `mysql_secure_installation`. C'est un binaire (= une commande, une application, un programme, ces mots désignent la même chose) qui sert à effectuer des configurations très récurrentes, on fait ça sur toutes les bases de données à l'install.  
C'est une question de sécu.

🌞 **Configuration élémentaire de la base**

- exécutez la commande `mysql_secure_installation`
  - plusieurs questions successives vont vous être posées
  - prenez le temps de lire et de répondre de façon avisée avec la sécurité en tête

---

🌞 **Préparation de la base en vue de l'utilisation par NextCloud**

- pour ça, il faut vous connecter à la base
- il existe un utilisateur `root` dans la base de données, qui a tous les droits sur la base
  - ce n'est pas le même `root` que sur l'OS, c'est celui qui existe dans la base de données
- si vous avez correctement répondu aux questions de `mysql_secure_installation`, vous ne pouvez utiliser le user `root` de la base de données qu'en vous connectant localement à la base
  - c'est à dire, pas à distance, pas depuis le réseau
- donc, sur la VM `db.tp2.cesi` toujours :

```bash
# Connexion à la base de données
# L'option -p indique que vous allez saisir un mot de passe
# Vous l'avez défini dans le mysql_secure_installation
$ sudo mysql -u root -p
```

Puis, dans l'invite de commande SQL :

```sql
# Création d'un utilisateur "nextcloud" dans la base, avec un mot de passe
# L'adresse IP correspond à l'adresse IP depuis laquelle viendra les connexions. Cela permet de restreindre les IPs autorisées à se connecter.
# Dans notre cas, c'est l'IP de web.tp2.cesi
# "meow" c'est le mot de passe :D
CREATE USER 'nextcloud'@'10.2.1.11' IDENTIFIED BY 'meow';

# Création de la base de donnée qui sera utilisée par NextCloud
CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

# On donne tous les droits à l'utilisateur nextcloud sur toutes les tables de la base qu'on vient de créer
GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.2.1.11';

# Actualisation des privilèges
FLUSH PRIVILEGES;
```

## 3. Test

➜ **On va tester que la base sera utilisable par NextCloud.**

Concrètement il va faire quoi NextCloud vis-à-vis de la base MariaDB ?

- se connecter sur le port où écoute MariaDB
- la connexion viendra de `web.tp2.cesi`
- il se connectera en utilisant l'utilisateur `nextcloud`
- il écrira/lira des données dans la base `nextcloud`

➜ Il faudrait donc qu'on teste ça, à la main, **depuis la machine `web.tp2.cesi`**.

Bah c'est parti ! Il nous faut juste un client pour nous connecter à la base depuis la ligne du commande : il existe une commande `mysql` pour ça.

🌞 **Installez sur la machine `web.tp2.cesi` la commande `mysql`**

- vous utiliserez la commande `dnf provides <COMMAND>` pour trouver dans quel paquet se trouve cette commande

🌞 **Tester la connexion**

- utilisez la commande `mysql` depuis `web.tp2.cesi` pour vous connecter à la base qui tourne sur `db.tp2.cesi`
- vous devrez préciser une option pour chacun des points suivants :
  - l'adresse IP de la machine où vous voulez vous connectez `db.tp2.cesi` : `10.2.1.12`
  - le port auquel vous vous connectez
  - l'utilisateur de la base de données sur lequel vous connecter : `nextcloud`
  - l'option `-p` pour indiquer que vous préciserez un password
    - vous ne devez PAS le préciser sur la ligne de commande
    - sinon il y aurait un mot de passe en clair dans votre historique, c'est moche
  - la base de données à laquelle vous vous connectez : `nextcloud`
- une fois connecté à la base en tant que l'utilisateur `nextcloud` :
  - effectuez un bête `SHOW TABLES;`
  - simplement pour vous assurer que vous avez les droits de lecture
  - et constater que la base est actuellement vide, super

> Je veux donc dans le compte-rendu la commande `mysql` qui permet de se co depuis `web.tp2.cesi` au service de base de données qui tourne sur `db.tp2.cesi`, ainsi que le `SHOW TABLES`.

---

C'est bon ? Ca tourne ? **On part sur Apache maintenant !**

# II. Setup Apache

> La section II et III sont clairement inspirés de [la doc officielle de Rocky pour installer NextCloud](https://docs.rockylinux.org/guides/cms/cloud_server_using_nextcloud/).

Comme annoncé dans l'intro, on va se servir d'Apache dans le rôle de serveur Web dans ce TP5. Histoire de varier les plaisirs è_é

![Linux is a tipi](./pics/linux_is_a_tipi.jpg)

## 1. Install Apache

### A. Apache

🌞 **Installer Apache sur la machine `web.tp2.cesi`**

- le paquet qui contient Apache s'appelle `httpd`

> Le d de `httpd` c'est pour *daemon*. Un *daemon* ça désigne un processus en tâche de fond. Tous les serveurs (web, FTP, SSH, etc...) font tourner un *daemon*.

---

🌞 **Analyse du service Apache**

- le service aussi s'appelle `httpd`
- lancez le service `httpd` et activez le au démarrage
- isolez les processus liés au service `httpd`
- déterminez sur quel port écoute Apache par défaut
- déterminez sous quel utilisateur sont lancés les processus Apache

---

🌞 **Un premier test**

- ouvrez le port d'Apache dans le firewall
- testez, depuis votre PC, que vous pouvez accéder à la page d'accueil par défaut d'Apache
  - avec une commande `curl`
  - avec votre navigateur Web

### B. PHP

NextCloud a besoin d'une version bien spécifique de PHP.  
Suivez **scrupuleusement** les instructions qui suivent pour l'installer.

🌞 **Installer PHP**

```bash
# ajout des dépôts EPEL
$ sudo dnf install epel-release
$ sudo dnf update
# ajout des dépôts REMI
$ sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
$ dnf module enable php:remi-7.4

# install de PHP et de toutes les libs PHP requises par NextCloud
$ sudo dnf install zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp
```

## 2. Conf Apache

➜ Le fichier de conf principal utilisé par Apache est `/etc/httpd/conf/httpd.conf`.  
Il y en a plein d'autres : ils sont inclus par le fichier de conf principal.

➜ Dans Apache, il existe la notion de *VirtualHost*. On définit des *VirtualHost* dans les fichiers de conf d'Apache.  
On crée un *VirtualHost* pour chaque application web qu'héberge Apache.

> "Application Web" c'est le terme de hipster pour désigner un site web. Disons qu'aujourd'hui les sites peuvent faire tellement de trucs qu'on appelle plutôt ça une "application" plutôt qu'un "site". Une application web donc.

➜ Dans le dossier `/etc/httpd/` se trouve un dossier `conf.d`.  
Des dossiers qui se terminent par `.d`, vous en rencontrerez plein (pas que pour Apache) **ce sont des dossiers de *drop-in*.**

Plutôt que d'écrire 40000 lignes dans un seul fichier de conf, on éclate la configuration dans plusieurs fichiers.  
C'est + lisible et + facilement maintenable.

**Les dossiers de *drop-in* servent à accueillir ces fichiers de conf additionels.**  
Le fichier de conf principal a une ligne qui inclut tous les autres fichiers de conf contenus dans le dossier de *drop-in*.

---

🌞 **Analyser la conf Apache**

- mettez en évidence, dans le fichier de conf principal d'Apache, la ligne qui inclut tout ce qu'il y a dans le dossier de *drop-in* nommé `conf.d/`

🌞 **Créer un VirtualHost qui accueillera NextCloud**

- créez un nouveau fichier dans le dossier de *drop-in*
  - attention, il devra être correctement nommé (l'extension) pour être inclus par le fichier de conf principal
- ce fichier devra avoir le contenu suivant :

```apache
<VirtualHost *:80>
  # on précise ici le dossier qui contiendra le site : la racine Web
  DocumentRoot /var/www/nextcloud/html/  

  # ici le nom qui sera utilisé pour accéder à l'application
  ServerName  web.tp2.cesi  

  <Directory /var/www/nextcloud/html/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
```

> N'oubliez pas de redémarrer le service à chaque changement de la configuration, pour que les changements prennent effet.

🌞 **Configurer la racine web**

- la racine Web, on l'a configurée dans Apache pour être le dossier `/var/www/nextcloud/html/`
- creéz ce dossier
- faites appartenir le dossier et son contenu à l'utilisateur qui lance Apache

> Jusqu'à la fin du TP, tout le contenu de ce dossier doit appartenir à l'utilisateur qui lance Apache. C'est strictement nécessaire pour qu'Apache puisse lire le contenu, et le servir aux clients.

🌞 **Configurer PHP**

- dans l'install de NextCloud, PHP a besoin de conaître votre timezone (fuseau horaire)
- pour récupérer la timezone actuelle de la machine, utilisez la commande `timedatectl` (sans argument ni option)
- modifiez le fichier `/etc/opt/remi/php74/php.ini` :
  - changez la ligne `;date.timezone =`
  - par `date.timezone = "<VOTRE_TIMEZONE>"`
  - par exemple `date.timezone = "Europe/Paris"`

# III. NextCloud

On dit "installer NextCloud" mais en fait c'est juste récupérer les fichiers PHP, HTML, JS, etc... qui constituent NextCloud, et les mettre dans le dossier de la racine web.

🌞 **Récupérer Nextcloud**

```bash
# Petit tip : la commande cd sans argument permet de retourner dans votre homedir
$ cd

# La commande curl -SLO permet de rapidement télécharger un fichier, en HTTP/HTTPS, dans le dossier courant (allez regarder ce que signifient ces options)
# Peut-être connaissiez vous déjà la commande wget. C'est simple : curl peut faire tout ce que fait wget. L'inverse n'est pas vrai.
$ curl -SLO https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip

$ ls
nextcloud-21.0.1.zip
```

🌞 **Ranger la chambre**

- extraire le contenu de NextCloud (beh ui on a récup un `.zip`)
- déplacer tout le contenu dans la racine Web
  - n'oubliez pas de gérer les permissions de tous les fichiers déplacés ;)
- supprimer l'archive

## 4. Test

Bah on arrive sur la fin du setup !

Si on résume :

- **un serveur de base de données : `db.tp2.cesi`**
  - MariaDB installé et fonctionnel
  - firewall configuré
  - une base de données et un user pour NextCloud ont été créés dans MariaDB
- **un serveur Web : `web.tp2.cesi`**
  - Apache installé et fonctionnel
  - firewall configuré
  - un VirtualHost qui pointe vers la racine `/var/www/nextcloud/html/`
  - NextCloud installé dans le dossier `/var/www/nextcloud/html/`

**Looks like we're ready.**

---

**Ouuu presque. Pour que NextCloud fonctionne correctement, il est préférable d'y accéder en utilisant un nom, et pas une IP.**  
On va donc devoir faire en sorte que, depuis votre PC, vous puissiez écrire `http://web.tp2.cesi` plutôt que `http://10.2.1.11`.

➜ Pour faire ça, on va utiliser **le fichier `hosts`**. C'est un fichier présents sur toutes les machines, sur tous les OS.  
Il sert à définir, localement, une correspondance entre une IP et un ou plusieurs noms.  

Emplacement du fichier `hosts` :

- MacOS/Linux : `/etc/hosts`
- Windows : `c:\windows\system32\drivers\etc\hosts`

---

🌞 **Modifiez le fichier `hosts` de votre PC**

- faites pointer le nom `web.tp2.cesi` vers l'IP `10.2.1.11`

🌞 **Tester l'accès à NextCloud et finaliser son install'**

- ouvrez votre navigateur Web sur votre PC
- rdv à l'URL `http://web.tp2.cesi`
- vous devriez avoir la page d'accueil de NextCloud
- ici deux choses :
  - les deux champs en haut pour créer un user admin au sein de NextCloud
  - le bouton "Configure the database" en bas
    - sélectionnez "MySQL/MariaDB"
    - entrez les infos pour que NextCloud sache comment se connecter à votre serveur de base de données
    - c'est les infos avec lesquelles vous avez validé à la main le bon fonctionnement de MariaDB (c'était avec la commande `mysql`)

---

**🔥🔥🔥 Baboom ! Un beau NextCloud.**

Naviguez un peu, faites vous plais', vous avez votre propre DropBox.

> Ptet le moment de prendre une pause pour laisser votre cerveau respirer ? :)

**Et après, go next : [Partie II : Sécurisation](./part2.md)**
