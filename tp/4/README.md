# Introduction Infrastructure as Code

<!-- vim-markdown-toc GitLab -->

* [I. Ansible](#ii-ansible)
    * [1. Un premier playbook](#1-un-premier-playbook)
    * [2. Création de playbooks](#2-création-de-playbooks)
        * [Déploiement base de données : MariaDB](#déploiement-base-de-données-mariadb)
        * [Déploiement comptes utilisateurs](#déploiement-comptes-utilisateurs)
        * [Configuration du firewall](#configuration-du-firewall)
* [II. Development techniques](#iii-development-techniques)
    * [Mise en place de tests](#mise-en-place-de-tests)

<!-- vim-markdown-toc -->

# I. Ansible

> Pour cette partie, il vous faudra une machine GNU/Linux (installé sur votre poste, ou utilisation d'une VM).

Ici, vous allez vous faire la main sur vos premiers *playbooks* Ansible.

Un peu de vocabulaire lié à Ansible :
* *inventory*
  * l'inventaire est la liste des hôtes
  * les hôtes peuvent être groupés dans un groupe qui porte un nom
* *task*
  * une tâche est une opération de configuration
  * par exemple : ajout d'un fichier, création d'un utilisateur, démarrage d'un service, etc.
* *role*
  * un rôle est un ensemble de tâches qui a un but précis
  * par exemple :
    * un role "apache" : il installe Apache, le configure, et le lance
    * un role "users" : il déploie des utilisateurs sur les machines
* *playbook*
  * un *playbook* est le lien entre l'inventaire et les rôles
  * un *playbook* décrit à quel noeud on attribue quel rôle

## 1. Un premier playbook

Juste pour jouer, mettre en place Ansible et appréhender l'outil, on va rédiger un premier playbook dans un seul fichier.

> Créez un répertoire dans votre répertoire personnel (à l'intérieur de `/home`) afin de stocker tous les fichiers Ansible. **Vous n'avez PAS BESOIN d'utiliser le dossier `/etc`**.

AVANT de pouvoir créer des *playbooks* afin d'automatiser le déploiement d'applications, configurations, etc., il est préférable de savoir déjà installer et configurer à la main ces éléments. N'hésitez pas à faire une installation manuelle des outils présentés, afin de bien comprendre l'exercice :)

---

On va ajouter simplement un serveur Web à notre machine virtuelle. Prérequis :

- machine "control node"
  - Ansible installé
  - connexion SSH sans mot de passe vers le "managed node"
  - fichiers Ansible (format `.yml`) dispos sur la machine
  - c'est la machine qui exécutera les commandes `ansible` afin de déployer de la conf
- machine "managed node"
  - Python installé
  - c'est la machine qui recevra la configuration envoyée depuis le "control node"

---

Une fois les prérequis en place, vous pouvez passez à la création des fichiers Ansible.

Créez ces fichiers dans un répertoire de travail (créez par exemple le dossier `/home/<USER>/ansible/` et vous créerez tous les fichiers `.yml` à l'intérieur.


* Créez un playbook minimaliste `nginx.yml` :

```yaml
---
- name: Install nginx
  hosts: cesi
  become: true

  tasks:
  - name: Add epel-release repo
    yum:
      name: epel-release
      state: present

  - name: Install nginx
    yum:
      name: nginx
      state: present

  - name: Insert Index Page
    template:
      src: index.html.j2
      dest: /usr/share/nginx/html/index.html

  - name: Start NGiNX
    service:
      name: nginx
      state: started
```

Et créez un inventaire `hosts.ini` :
```
[cesi]
<VM_IP>
```

Enfin, créez un fichier `index.html.j2` :
```
Hello from {{ ansible_default_ipv4.address }}
```

* Lancez le playbook !
```
$ ansible-playbook -i hosts.ini nginx.yml
```

**Vérifier le bon fonctionnement du site web !**

## 2. Création de playbooks

Il y a trois playbooks à réaliser dans cette partie : `maria.yml`, `users.yml` et `firewall.yml`.

**Répartissez-vous les tâches au sein du groupe. Travaillez chacun sur vos branches git. Une fois chaque parties terminées, mettez votre travail en commun sur la branche master.**

### Déploiement base de données : MariaDB

Créez un playbook `maria.yml` qui :
* installe MariaDB
* lance MariaDB
* ajoute une base de données

### Déploiement comptes utilisateurs

Puis un playbook `users.yml` qui :
* ajoute un utilisateur
* lui déposer une clé SSH publique afin de pouvoir s'y connecter
  * [utilisez le module `authorized_key`](https://docs.ansible.com/ansible/latest/modules/authorized_key_module.html)

### Configuration du firewall

Enfin un playbook `firewall.yml` qui :
* ouvre le port 80 et le port 22
* s'assure que le port 8000 est fermé

**Vérifier que tout fonctionne.**

Pousser une version fonctionnelle des *playbooks* sur la branche *master* (en faisant un *merge*)

# II. Development techniques

## Mise en place de tests

Dans cette dernière étape, on va mettre an place des tests très simplistes sur ce qu'on vient de produire afin d'appréhender le déroulement de tests automatisés.

Grâce à Gitlab, il est possible de mettre en palce des tests automatisés sur le contenu du dépôt. 

A titre d'exemple, nous allons tester :
* la bonne syntaxe des fichiers `.yml`

La configuration des tests avec Gitlab se fait simplement *via* l'ajout d'un fichier `.gitlab-ci.yml` à la racine du dépôt. Un exemple pourrait ressembler à :
```yml
stages:
  - first_test

first-test:
  stage: first_test
  image: debian
  script:
    - echo 'toto'
```

Une fois ce fichier ajouté au dépôt, le test devrait être exécuté automatiquement à chaque push sur la branche contenant le fichier `.gitlab-ci.yml`.

Les tests sont visibles dans l'onglet `CI/CD` du panneau latéral. **Vérifier que le test s'exécute correctement.**

> **Les tests sont exécutés dans des conteneurs Docker** : c'est pour ça que l'on précise une `image`.

---

A réaliser :
* trouver une commande qui permet de tester la bonne syntaxe d'un fichier yml
* ajouter dans le `.gitlab-ci.yml` l'exécution de ces commandes afin de vérifier les fichiers de votre dépôt Git
