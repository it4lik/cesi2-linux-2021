# Partie 3 : Bonus

## I. Echange de clés SSH

Configurez un échange de clés SSH, pour que vous vous connectez à la VM sans donner de passe, mais uniquement en présentant une utilisant un échange de clés.

## II. Protéger le petit serveur web

Premièrement : faites en sorte que le serveur web python lancé par votre service n'écoute que sur l'adresse de loopback (il faut modifier la ligne `python3 -m http.server`).

Dans un deuxième temps, affinez la configuration de NGINX déjà installé en partie 2 :

- désactiver la page d'accueil par défaut
- faire en sorte que NGINX agisse comme un reverse proxy vers le service web
  - il faudra utiliser la clause `proxy_pass` de la configuration NGINX

Enfin : vous mettrez en place du HTTPS. Les clients devront accéder au site web uniquement en HTTPS. Pour cela :

- générez un certificat et une clé auto-signés
- affinez la configuration de NGINX pour qu'il utilise ce certificat afin de chiffrer l'accès au site Web
