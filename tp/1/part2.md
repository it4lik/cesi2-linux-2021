
# II. Gestion de services

Un *service* c'est simplement un programme lancé par le système, par l'OS. Cela amène plusieurs avantages que nous allons aborder dans cette partie :

- centralisation des logs de ces programmes
- gestion unifiée (pour démarrer, stopper, activer au démarrage de la machine, etc.)
- et d'autres que l'on explorera pas pour le moemnt

## Sommaire

- [II. Gestion de services](#ii-gestion-de-services)
  - [Sommaire](#sommaire)
  - [1. Analyse du service SSH](#1-analyse-du-service-ssh)
  - [2. Modification du service SSH](#2-modification-du-service-ssh)
  - [3. Service Web](#3-service-web)
  - [4. Votre propre service](#4-votre-propre-service)
    - [A. Présentation](#a-présentation)
    - [B. Création d'un service](#b-création-dun-service)
    - [C. Affiner la configuration du service](#c-affiner-la-configuration-du-service)

## 1. Analyse du service SSH

🌞 **Afficher le statut du service SSH**

- avec une commande `systemctl status sshd`
- vérifiez qu'il est bien actif

🌞 **Déterminer sur quel port le service SSH écoute**

- avec une commande `ss`
  - on utilise souvent les options `sudo ss -lutpn`
  - je vous invite fortement à consulter le `man` pour voir ce que signifient ces options

> La commande `netstat` est obsolète sous GNU/Linux si vous la connaissiez.

🌞 **Déterminer l'utilisateur qui a lancé le processus SSH**

- avec une commande `ps -ef`

## 2. Modification du service SSH

🌞 **Changez le port d'écoute du service SSH**

- vous allez devoir modifier la configuration du service SSH dans le fichier `/etc/ssh/sshd_config`
- il sera ensuite nécessaire de redémarrer le service SSH avec une commande `systemctl restart sshd`
- il faudra aussi ouvrir ce port dans le firewall de la machine
  - voir juste en dessous pour les commandes concernant le firewall

Pour manipuler le firewall de Rocky, on utilise la commande `firewall-cmd` :

```bash
# Consulter l'état du firewall
$ firewall-cmd --list-all

# Ouvrir un port
$ firewall-cmd --permanent --add-port=PORT
# Par exemple
$ firewall-cmd --permanent --add-port=80/tcp

# Fermer un port
$ firewall-cmd --permanent --remove-port=PORT
# Par exemple
$ firewall-cmd --permanent --remove-port=80/tcp

# Reload le firewall pour que les changements prennent effet
$ firewall-cmd --reload
```


🌞 **Vérification**

- vérifiez le changement avec une commande `ss`
- connectez vous depuis votre machine en SSH vers la VM, en précisant le nouveau port à utiliser

## 3. Service Web

🌞 **Installez le paquet NGINX**

- le paquet s'appelle `nginx`
- il faudra utiliser la commande `dnf install nginx` pour l'installer

🌞 **Lancez le service NGINX**

- avec une commande `systemctl start`

🌞 **Gestion de firewall**

- déterminez avec une commande `ss` sur quel port écoute NGINX
- ouvrez ce port dans le firewall pour autoriser la visite du service

🌞 **Vérifier que le service est accessible**

- depuis votre PC, avec un navigateur, visitez l'IP de la VM
- avec la commande `curl` depuis un terminal

## 4. Votre propre service

### A. Présentation

Finalement un serveur Web c'est juste un truc qui permet de parcourir un dossier, et qui nous fait télécharger son contenu. C'est juste particulièrement adapté au Web grâce au protocole HTTP.

On peut lancer de façon très simpliste grâce à Python :

```bash
# Lancer un serveur web en une commande grâce à Python
# Il est tout pourri et n'est pas fait pour de la production, mais ça permet de faire des ptits trucs
# Lancé depuis un dossier donné, il servira les fichiers de ce dossier
$ python3 -m http.server 8888 # 8888 c'est le port où il va écouter
```

🌞 **Essayez de lancer le serveur Python à la main**

- si vous n'avez pas `python` il faudra l'installer
- testez que ça fonctionne en le visitant avec votre navigateur ou commande `curl` depuis votre PC
- n'oubliez pas d'ouvrir le port firewall

---

Pour créer un service, il suffit de créer un fichier dans le dossier `/etc/systemd/system/`, avec l'extension `.service`. Il devra avoir la syntaxe minimale suivante :

```bash
[Unit]
Description=<DESCRIPTION>

[Service]
ExecStart=<COMMAND>

[Install]
WantedBy=multi-user.target
```

Une fois l'unité de service créée, il faut demander à *systemd* de relire les fichiers de configuration :

```bash
$ sudo systemctl daemon-reload
```

Enfin, on peut interagir avec notre unité :

```bash
$ sudo systemctl status <UNIT> # quand le fichier s'appelle <UNIT>.service
$ sudo systemctl start <UNIT>
$ sudo systemctl enable <UNIT>
```

### B. Création d'un service

🌞 **Créez un service `web.service`**

- il devra exécuter la commande `/usr/bin/python3 -m http.server 8888`
  - vérifiez que la commande `python3` se trouve bien dans ce dossier
    - vous pouvez utiliser `which python3` pour voir dans quel chemin la commande se trouve
  - si ce n'est pas le cas, vous pouvez déterminer où est la commande `python3` en exécutant : `which python3`
- vérifiez qu'il fonctionne
  - avec une commande `systemctl status`
  - en visitant le service depuis votre PC

> N'oubliez pas d'exécuter `sudo systemctl daemon-reload` à chaque modification d'un fichier de service. Puis de redémarrer le service concerné par la modif si nécessaire.

### C. Affiner la configuration du service

🌞 **Préparez l'environnement**

- créez un utilisateur `web`
- créez un dossier `/srv/web/`
  - créez un fichier texte avec un contenu simpliste à l'intérieur dans le dossier `/srv/web/`
  - utilisez une commande `chown` pour que ce dossier et le fichier appartiennent à l'utilisateur `web`
  - vérifiez avec une commande `ls -al` que le fichier a bien changé de propriétaire

🌞 **Modifiez l'unité de service `web.service` créée précédemment en ajoutant les clauses :**

- `User=` afin de lancer le serveur avec l'utilisateur `web` dédié
- `WorkingDirectory=` qui pointe vers le dossier `/srv/web/`
- ces deux clauses sont à positionner dans la section `[Service]` de votre unité

> N'oubliez pas d'exécuter `sudo systemctl daemon-reload` à chaque modification d'un fichier de service. Puis de redémarrer le service concerné par la modif si nécessaire.

🌞 **Vérifiez le bon fonctionnement avec une commande `curl`** depuis votre PC

- vous devriez pouvoir télécharger le fichier créé dans `/srv/web/`
