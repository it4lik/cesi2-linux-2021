# CESI Linux 2021

Support de cours de la session Linux CESI Décembre 2021.

## [Memos](./cours/README.md)

- [Mémo réseau Rocky](./cours/memo/rocky_network.md)

## [Cours](./cours/README.md)

- [Intro Crypto](./cours/intro_crypto/README.md)
- [SSH](./cours/ssh/README.md)


## [TP](./tp/README.md)

- [TP1 : On se met dans le bain](./tp/1/README.md)
- [TP2 : Un service entouré d'un écosystème riche](./tp/2/README.md)
