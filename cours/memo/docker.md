# Memo Docker 

* [Intro](#intro)
  * [Lancer un conteneur](#lancer-un-conteneur)
  * [Agir sur les conteneurs existants](#agir-sur-les-conteneurs-existants)
  * [Récupérer une image externe](#récupérer-une-image-externe)
  * [Lister les images locales](#lister-les-images-locales)
  * [Partage de port](#partage-de-port)
  * [Volume](#volume)
  * [Récupérer un terminal dans un conteneur existant](#récupérer-un-terminal-dans-un-conteneur-existant)
  * [Récupérer les logs d'un conteneur existant](#récupérer-les-logs-d'un-conteneur-existant)
* [docker-compose](#docker-compose)
* [Images](#images)
  * [Exemple](#exemple)
  * [Build](#build)

#Intro

Docker est un outil de conteneurisation. Cross-platform, vous pourrez l'installer et le faire fonctionner nativement sous GNU/Linux, MacOS ou Windows.

Un "conteneur" c'est un environnement isolé qui vit au sein de votre système. C'est semblable à une VM dans l'utilisation (MAIS avec la virtualisation du hardware (CPU, RAM, dique, etc.) en moins).

Un conteneur est **obligé** d'exécuter un processus, une commande. S'il n'exécute rien, il quitte (dans le cas où on ne lance aucune commande ou si la commande qu'on a lancé dans le conteneur s'est terminée).

---

Pour créer un conteneur, on instancie ce qu'on appelle une "image". On peut l'instancier plusieurs fois: on peut créer plusieurs conteneurs à partir d'une seule image.

Une image, quant à elle, est construite à partir d'un Dockerfile.

Le workflow classique :

1. Création d'un Dockerfile
2. "Build" du Dockerfile, afin de créer une image
3. Instanciation de l'image pour créer un conteneur

> Attention, une image Docker est complètement statique, il est impossible de la modifier une fois créée. Pour mettre à jour une image donnée, on en reconstruit une nouvelle.

# Commandes

## Lancer un conteneur

```bash
$ docker run <OPTIONS> <IMAGE> <COMMANDE>

# Lancement d'un conteneur simple, basé sur Debian, qui quitte tout de suite
$ docker run debian

# Lancement d'un conteneur simple, basé sur Debian, qui exécute un sleep en tâche de fond
# Si ce sleep se termine, le conteneur quitte
$ docker run -d debian sleep 9999

# Lancement d'un conteneur simple, basé sur Debian, qui exécute un terminal
# Si vous quittez le terminal, le conteneur quitte
$ docker run -it debian bash
```

## Agir sur les conteneurs existants

```bash
# Lister les conteneurs actifs
$ docker ps

# Lister les conteneurs actifs et inactifs
$ docker ps -a

# Supprimer un conteneur
$ docker rm -f <NAME_OR_ID>
```

## Récupérer une image externe

On peut récupérer, depuis la ligne de commande, des images hébergées sur des registres Docker externes. Par défaut, la commande `docker` va chercher les images qu'on lui demande sur le [Docker Hub](https://hub.docker.com) mais il est possible de faire appel à un registre spécifique.

Récupération d'images externes :

```bash
# Récupère l'image debian sur le Docker Hub
$ docker pull debian

# Récupère une image debian en version Jessie sur le Docker Hub
$ docker pull debian:jessie
```

## Lister les images locales

```bash
$ docker images
```

## Partage de port

```bash
# Partager le port 8080 de l'hôte vers le port 80 du conteneur
$ docker run -p 8080:80 nginx
```

## Volume

Les *volumes* permettent de partager un dossier entre la machine hôte le conteneur.

Aucune copie ou synchronisation n'est mise en place : le dossier de l'hôte et du conteneur sont les mêmes.

Ainsi, dans le contexte où un conteneur est éphémère (il est créé, détruit, puis recréé souvent), la mécanique de *volumes* permet d'avoir des **données persistantes**, qui subsisteront après destruction et recréation des conteneurs.

```bash
# Partager le fichier /tmp/test.html qui existe déjà sur l'hôte
# Et le monter dans le conteneur au path /var/www/html/test.html
$ docker run -v /tmp/test.html:/var/www/html/test.html debian
```

## Récupérer un terminal dans un conteneur existant

```bash
$ docker exec -it <NAME_OR_ID> <SHELL>

# Par exemple
$ docker exec -it <NAME_OR_ID> bash
$ docker exec -it <NAME_OR_ID> sh
```

## Récupérer les logs d'un conteneur existant

```bash
$ docker logs <NAME_OR_ID>

# L'ajout de l'option -f permet de suivre l'évolution des logs en direct
$ docker logs -f <NAME_OR_ID>
```

# docker-compose

`docker-compose` est un outil additionnel, développé aussi par la société Docker.

> Il est à télécharger en plus de Docker, cf la doc officielle.

`docker-compose` permet de lancer un ou plusieurs conteneurs, en les déclarant dans un fichiers `.yml`.

Les bénéfices :

- les commandes `docker run` sont parfois longues et difficilement maintenables
  - plutôt qu'un ou plusieurs `docker run`, on utilise un simple fichier texte, au format `.yml`
- lancer plusieurs conteneurs est tout aussi siple qu'en lancer un seul
  - un fichier `docker-compose.yml` peut contenir la déclaration de plusieurs conteneurs.

Exemple de `docker-compose.yml`, il monte Apache + MySQL + NGINX afin de servir un NextCloud :

> Ce `docker-compose.yml` a besoin de plusieurs dossiers pour fonctionner. Voir [le dossier d'exemple](../cours/docker/)

```yml
version: '3.8'

services:
  toto:
    image: nginx
  nextcloud_db:
    image: mariadb
    container_name: nextcloud_db
    restart: always
    command: --transaction-isolation=READ-COMMITTED --binlog-format=ROW --innodb-file-per-table=1 --skip-innodb-read-only-compressed
    volumes:
      - ./db:/var/lib/mysql
    environment:
      - MYSQL_ROOT_PASSWORD=$NEXTCLOUD_MYSQL_ROOT_PASSWORD
      - MYSQL_DATABASE=$NEXTCLOUD_MYSQL_DATABASE
      - MYSQL_USER=$NEXTCLOUD_MYSQL_USER
      - MYSQL_PASSWORD=$NEXTCLOUD_MYSQL_PASSWORD
    networks:
      nextcloud:

  nextcloud_app:
    image: nextcloud
    restart: always
    volumes:
      - ./data:/var/www/html
    environment:
      - MYSQL_HOST=nextcloud_db
      - MYSQL_DATABASE=$NEXTCLOUD_MYSQL_DATABASE
      - MYSQL_USER=$NEXTCLOUD_MYSQL_USER
      - MYSQL_PASSWORD=$NEXTCLOUD_MYSQL_PASSWORD
    networks:
      nextcloud:
        aliases:
          - web.tp3.cesi
  proxy:
    image: nginx
    ports:
      - $NEXTCLOUD_HTTP_PORT:80
      - $NEXTCLOUD_HTTPS_PORT:443
    volumes:
      - ./nginx/nginx.conf:/etc/nginx/nginx.conf
      - ./nginx/web.tp3.cesi.key:/etc/nginx/ssl/web.tp3.cesi.key
      - ./nginx/web.tp3.cesi.crt:/etc/nginx/ssl/web.tp3.cesi.crt
    networks:
      nextcloud:

networks:
  nextcloud:
```

# Dockerfile

## Exemple

Exemple de Dockerfile :

```Dockerfile
FROM debian:jessie

# RUN permet d'exécuter des commandes shell
RUN apt-get update -y

RUN apt-get install -y vim

# COPY permet de récupérer un répertoire sur notre machine, la machine de build, et de le mettre dans l'image
COPY test /opt/test

# Définit un répertoire de travail depuis lesquelles toutes les commandes suivantes seront exécutées
# Y compris le ENTRYPOINT, pratique pour lancer une commande depuis la racine de votre projet
WORKDIR /opt/test

ENTRYPOINT [ "/bin/sleep", "9999" ]
```

## Build

Pour build un Dockerfile en une image :
```bash
$ ls 
Dockerfile

$ docker build -t <NAME> <CONTEXT>

# Par exemple, si le Dockerfile est dans le répertoire courant
$ docker build -t my_image .
```

